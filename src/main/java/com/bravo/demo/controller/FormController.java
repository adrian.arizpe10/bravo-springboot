package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FormController {
    @GetMapping("/join") // URL Path
    public String showJoinForm(){ //method attached to URL
        return "join"; // HTML File Name
    }
    @PostMapping("/join")//post mapping is used to post data to the view
    public String joinCohort(@RequestParam(name = "cohort")String cohort, Model model){
//        @RequestParam annotation is used to read the form data and bind it automatically to the parameter variable present in the provided method.
        //we have used "cohort" to represent example
        model.addAttribute("cohort", "Welcome to " + cohort + "!");
        return "join";
    }
}
