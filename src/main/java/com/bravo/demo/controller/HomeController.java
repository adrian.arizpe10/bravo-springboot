//package com.bravo.demo.controller;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//@Controller
//public class HomeController {
//    @GetMapping(path = "/")
//    @ResponseBody// this will configure the method as the response
//    public String landingPage(){
//        return "Welcome to my landing page!";
//    }
//
//    @GetMapping(path = "/home")//in this example we dont have the @ResponseBody because html (home) will be returned
//    public String welcome(){
//        return "home";
//    }
//
//    @GetMapping("/hello/{name}")
//    public String sayHello(@PathVariable String name, Model model){ // Model is a class from the built in spring.ui within framework NOT part of the MVC design pattern
//        model.addAttribute("name", name);
//        return "hello";
//    }
//}
