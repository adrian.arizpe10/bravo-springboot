package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller // this identifies this class to be the communication to the url path within the spring MVC design pattern
public class HelloSpringController {
    @GetMapping("/hello") //GetMapping annotation to define what url map this method identifies with
    @ResponseBody // defines what the response will be once the URL is given
    public String hello(){ // the method called on the response body annotation
        return "Hello from springBoot!";
    }

    @GetMapping("/goodbye")
    @ResponseBody
    public String goodbye(){
        return "Goodbye from SpringBoot!";
    }
    //Using RequestMapping with path variable from Spring spring framework.
    //Pathing variables - Using {number} to pass in a variable
    @RequestMapping(path = "/favorite/{number}")
    @ResponseBody
    public String sayFavNum(@PathVariable int number){ // @PathVariable defines the variable object to the @RequestMapping url path
        return "Looks like your favorite number is: " + number;
    }

    @RequestMapping(path = "/increment/{number}", method = RequestMethod.GET)//method class inside SpringBoot Framework
    @ResponseBody // this annotation identifies the method will be the response.
    public String addOne(@PathVariable int number){ // @PathVariable passing in variable to identify to @RequestMapping.
        return number + " plus one is " + (number + 1) + "!";
    }//above is the return statement to return on method logic.
}
