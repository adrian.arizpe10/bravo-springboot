package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FormController2 {
    @GetMapping("/show-form")
    public String showForm2(){
        return "company-form";
    }

    //mapping to process post the information from the HTML form
    @PostMapping("/process-form")
    public String processForm(@RequestParam(name = "firstName")String firstName,
                              @RequestParam(name = "lastName") String lastName,
                              @RequestParam(name = "city") String city,
                              @RequestParam(name = "state") String state,
                              @RequestParam(name = "email") String email,
                              Model infoModel) {
        //@RequestParam to get form data
        infoModel.addAttribute("name", "Name: " + firstName + " " + lastName);
        infoModel.addAttribute("hometown", "Hometown: " + city + " " + state);
        infoModel.addAttribute("contact", "Email: " + email);
        //The first parameter in .addAttribute matches up with the html variable that is used in the following syntax IN HTML <p th:text="${contact}"></p>
        return "show-form-submit";
    }
}
