package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MathController {

    @GetMapping(path = "/math/{number}/and/{number2}")
    @ResponseBody
    public String math(@PathVariable int number, @PathVariable int number2){
        return "Your number + number2 is: " + (number + number2);
    }
}
