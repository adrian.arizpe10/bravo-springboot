package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class casinoRoll {

    @GetMapping("/casino-roll")
    public String page(){
        return "casinoRollG";
    }

    @PostMapping("/take-num")
    public String roll(@RequestParam(name = "guess") int guess, Model model){
        int roll;
        roll = (int) (Math.floor(Math.random() * 6) + 1);
        model.addAttribute("random", "Random Number: " + roll);
        model.addAttribute("guess", "Guess: " + guess );
        if (guess == roll){
            model.addAttribute("final", "You Guessed Correctly!");
        } else {
            model.addAttribute("final", "Oops! Try Again...");
        }
        return "roll";
    }

}
