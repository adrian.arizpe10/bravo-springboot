package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {
    @GetMapping(path = "/hello/{user}")
    @ResponseBody // this will configure the method as the response
    public String timmy(@PathVariable String user){
        return "Hello " + user;
    }
}
