package com.bravo.demo.repositories;

import com.bravo.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface userRepository extends JpaRepository<User, Integer> {

}
//create controller -> model -> repo -> run app -> refresh database -> look for table
